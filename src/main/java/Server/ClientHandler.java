package Server;

import Exceptions.GeneralException;
import Exceptions.UserException;
import Exercises.*;
import Trainings.Training;
import Trainings.TrainingDay;
import Trainings.TrainingDayManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.UserManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by jozef on 6.5.2017.
 */

public class ClientHandler extends Thread {
    private volatile boolean running;

    private enum ACTION {
        UNKNOWN, REGISTRATION, LOGIN, SAVE, SAVE_AS, LOAD, LOAD_NAMES,EXIT, ADD,DELETE,INIT,DELETE_TRAINING
    }
    private BufferedReader in;
    private PrintWriter out;

    /**
     * Sets the string to the variables in the enum.
     * @param s
     * @return action
     */
    private ACTION parseCmd(String s)
    {
        for(ACTION action : ACTION.values())
        {
            if(action.name().equals(s))
                return action;
        }
        return ACTION.UNKNOWN;
    }

    private void terminate()
    {
        running = false;
    }

    private Socket socket;
    private int clientNumber;
    private UserManager userManager;
    private ExerciseManager exerciseManager;
    private TrainingDayManager trainingDayManager;
    private int index;

    /**
     * Returns a true if someone is logged in.
     * @return isLoggedIn
     */
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    private boolean isLoggedIn = false;

    /**
     * Constructor
     * Creates a client-server connection,
     * throws an exception if a problem occurs.
     * @param socket
     * @param clientNumber
     * @throws IOException
     */
    public ClientHandler(Socket socket, int clientNumber) throws IOException {
        try {
            this.running = true;
            if (clientNumber >= 5)
                terminate();
            this.socket = socket;
            this.clientNumber = clientNumber;
            in = new BufferedReader(
                    new InputStreamReader(this.socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);
            userManager = new UserManager();
            if (!running)
                out.println("CLOSED");
            else
                out.println("OK");
        }
        catch (SQLException ex)
        {
            out.println("Error: Cannot connect to database.");
        }
    }

    /**
     * Communicating with a client using a command line,
     * throws an exception if a problem occurs.
     */
    public void run() {
        try {

            // Decorate the streams so we can send characters
            // and not just bytes.  Ensure output is flushed
            // after every newline.

            // Send a welcome message to the client.

            // Get messages from the client, line by line; return them
            // capitalized
            while (running) {
                String input = in.readLine();
                log(input);
                try {
                    switch (parseCmd(input)) {
                        case UNKNOWN:
                            log("Unknown call. Exiting");
                            System.exit(1);
                            break;
                        case REGISTRATION:
                            doRegistration();
                            break;
                        case LOGIN:
                            login();
                            break;
                        case SAVE:
                            save();
                            break;
                        case SAVE_AS:
                            save();
                            break;
                        case LOAD:
                            load();
                            break;
                        case LOAD_NAMES:
                            loadNames();
                            break;
                        case ADD:
                            addExercise();
                            break;
                        case DELETE:
                            deleteExercise();
                            break;
                        case INIT:
                            init();
                            break;
                        case DELETE_TRAINING:
                            delete();
                            break;
                        case EXIT:
                            log("Exiting");
                            System.exit(0);
                            break;
                    }
                }
                catch (UserException | GeneralException ex)
                {
                    //out.write("UserError\n");
                    out.println("Error "+ ex.getMessage());
                    log(ex.getMessage());
                }
                catch (SQLException ex)
                {
                    //out.write("SQLError\n");
                    out.println("Error occurred during work with a database.");
                    log("Error occurred during work with a database.");
                }
                catch (NoSuchAlgorithmException ex)
                {
                    //out.write("AlgorithmError\n");
                    out.println("Error No such algorithm - encrypting");
                    log("No such algorithm - encrypting");
                }
            }
        } catch (IOException e) {
            log("Error handling client# " + clientNumber + ": " + e);
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                log("Couldn't close a socket, what's going on?");
            }
            log("Connection with client# " + clientNumber + " closed");
            Server.numberOfClients--;
        }
    }

    private void delete() throws IOException,UserException,SQLException {
        String exercise = in.readLine();
        trainingDayManager.deleteName(exercise);
        out.println("OK");
        log("Training " + exercise + " deleted");
    }

    private void initialize() throws IOException
    {
        try {
            exerciseManager = new ExerciseManager(index);
            trainingDayManager = new TrainingDayManager(index);
            log("New connection with client# " + clientNumber + " at " + socket);
        }
        catch (UserException | GeneralException ex)
        {
            //out.write("UserError\n");
            out.println(ex.getMessage());
            log(ex.getMessage());
        }
        catch (SQLException ex)
        {
            //out.write("SQLError\n");
            out.println("Error occurred during work with a database.");
            log("Error occurred during work with a database.");
        }
    }

    private void init() throws UserException,IOException {
        for(Exercise exercise : exerciseManager.getExercises())
        {
            if(exercise.getName().equals(""))
                throw new UserException("Enter the name");
            String type = "";
            if (exercise instanceof SetsExercise) {
                type = "SETS";
            } else if (exercise instanceof DistanceExercise) {
                type = "DISTANCE";
            } else if (exercise instanceof TimeExercise) {
                type = "TIME";
            }
            out.println(exercise.getName());
            out.println(type);
        }
        out.println("STOP");
    }

    /**
     * It deletes selected exercise.
     * @throws IOException
     * @throws SQLException
     * @throws UserException
     */
    private void deleteExercise() throws IOException,SQLException,UserException {
        String exercise = in.readLine();
        exerciseManager.deleteExercise(exercise);
        out.println("OK");
        log("Exercise " + exercise + " deleted");

    }

    /**
     * It adds new excercise to database,
     * throws an exception if a problem occurs.
     * @throws IOException
     * @throws SQLException
     * @throws UserException
     */
    private void addExercise() throws IOException,SQLException,UserException {
        String exercise = in.readLine();
        String type = in.readLine();
        boolean time = false, distance = false, sets = false;
        if(type.equals("TIME"))
            time = true;
        else if(type.equals("DISTANCE"))
            distance = true;
        else if(type.equals("SETS"))
            sets = true;
        else
            throw new IOException();
        exerciseManager.addExercise(exercise,sets,time,distance,true);
        out.println("OK");
        log("Exercise " + exercise + " added.");
    }

    /**
     * It logs in the user when he/she is already logged in throws an exception.
     * @throws IOException
     * @throws SQLException
     * @throws UserException
     * @throws NoSuchAlgorithmException
     */
    private void login() throws IOException,SQLException, UserException,NoSuchAlgorithmException {
        if(!isLoggedIn) {
            this.index = userManager.doLogin(in.readLine(),in.readLine());
            isLoggedIn = true;
            out.println("OK");
            out.println(this.index);
            log("Logged in");
            initialize();

        }
        else
        {
            in.readLine();in.readLine();
           throw new UserException("Already logged in");
        }
    }

    /**
     * It registers user according to the entered data
     * @throws IOException
     * @throws SQLException
     * @throws UserException
     * @throws NoSuchAlgorithmException
     */
    private void doRegistration() throws IOException,SQLException, UserException,NoSuchAlgorithmException
    {
        String name,surname, username, mail,  password,  passwordAgain;
        LocalDate birthday;
        Boolean male,agree;
        Double height, weight;
        name = in.readLine(); surname = in.readLine(); username = in.readLine(); mail = in.readLine(); password = in.readLine(); passwordAgain = in.readLine();
        String tmp = in.readLine();
        birthday = tmp.equals("") ? null : LocalDate.parse(tmp);
        male = Boolean.valueOf(in.readLine());
        height = Double.parseDouble(in.readLine());
        weight = Double.parseDouble(in.readLine());
        agree = Boolean.valueOf(in.readLine());
        if(isLoggedIn)
        {
            throw new UserException("Already logged in");
        }
        userManager.doRegistration(name,surname,username,mail,password,passwordAgain,birthday,male,height,weight,agree);
        out.println("OK");
        log("Registration successful");

    }

    private int tryParse(String s)
    {
        int num;
        try {
            num = Integer.parseInt(s);
        }
        catch (NumberFormatException ex)
        {
            num = 0;
        }
        return num;
    }

    /**
     * Saves the exercise into the database by type of exercise,
     * throws an exception if a problem occurs.
     * @throws IOException
     * @throws SQLException
     * @throws UserException
     */
    private void save() throws IOException,SQLException, UserException,GeneralException
    {
        String training = in.readLine(), date = in.readLine();
        while(true)
        {
            String s = in.readLine();
            if(s == null || s.equals("STOP"))
                break;
            if(s.charAt(0) == 'S')
            {
                String name = s.substring(2);
                //save to sets table
                String sets,reps;
                sets = in.readLine(); reps = in.readLine();
                if(sets == null || reps == null)
                    throw new IOException();
                int intSets = tryParse(sets);
                int intReps = tryParse(reps);
                trainingDayManager.getTrainingDay().addTraining(new Training(new SetsExercise(name,intReps,intSets)));
            }
            else if(s.charAt(0) == 'D')
            {
                String name = s.substring(2);
                //save to distance table
                String distance,minutes;
                distance = in.readLine(); minutes = in.readLine();
                if(distance == null || minutes == null)
                    throw new IOException();
                int intDistance = tryParse(distance);
                int intMinutes = tryParse(minutes);
                trainingDayManager.getTrainingDay().addTraining(new Training(new DistanceExercise(name,intDistance,intMinutes)));
            }
            else if(s.charAt(0) == 'T')
            {
                //save to time table
                String name = s.substring(2);
                String minutes;
                minutes = in.readLine();
                if(minutes == null)
                    throw new IOException();
                int intMinutes = tryParse(minutes);
                trainingDayManager.getTrainingDay().addTraining(new Training(new TimeExercise(name,intMinutes)));
            }

        }
        try {
        trainingDayManager.save(training,LocalDate.parse(date));
        }
        catch (SQLException ex)
        {
            throw new GeneralException("Name already used");
        }
        out.println("OK");
    }

    private void loadNames() throws SQLException{
        ResultSet rs = trainingDayManager.loadNames();
        if (!rs.isBeforeFirst() )
            throw new SQLException("");
        while(rs.next()) {
            out.println(rs.getString("name"));
            out.println(rs.getDate("date").toLocalDate());
        }
        out.println("STOP");
    }

    private void load() throws IOException,SQLException, UserException,GeneralException
    {
        String name = in.readLine();
        trainingDayManager.load(name);
        //out.println(trainingDayManager.getTrainingDay().getName());
        out.println(trainingDayManager.getTrainingDay().getDate());
        for(Training training : trainingDayManager.getTrainingDay().getTrainings())
        {
            Exercise exercise = training.getCurrentExercise();
            if (exercise instanceof SetsExercise) {
                out.println("S " + exercise.getName());
                out.println(((SetsExercise) exercise).getReps());
                out.println(((SetsExercise) exercise).getSets());
            } else if (exercise instanceof DistanceExercise) {
                out.println("D " + exercise.getName());
                out.println(((DistanceExercise) exercise).getDistance());
                out.println(((DistanceExercise) exercise).getMinutes());
            } else if (exercise instanceof TimeExercise) {
                out.println("T " + exercise.getName());
                out.println(((TimeExercise) exercise).getMinutes());
            }
        }
        trainingDayManager.getTrainingDay().resetTrainings();
        out.println("STOP");
    }

    private void log(String message) {
        System.out.println(message);
    }
}
