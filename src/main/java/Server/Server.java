package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by jozef on 6.5.2017.
 * Mutlithread Server application
 * Each request is handled to ClientHandler
 */
public class Server {

    /**
     * Creates a socket and assigns threads to users,
     * throws an exception if a problem occurs.
     * @param args
     * @throws Exception
     */
    static int numberOfClients = 0;

    public static void main(String[] args) throws Exception {
        System.out.println("Server is running.");
        try (ServerSocket listener = new ServerSocket(9898)) {
            while (true) {
                new ClientHandler(listener.accept(), numberOfClients++).start();
            }
        }
    }

    /**
     * A private thread to handle capitalization requests on a particular
     * socket.  The client terminates the dialogue by sending a single line
     * containing only a period.
     */

}