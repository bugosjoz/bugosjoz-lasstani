package Controllers;

import Client.Client;
import Exceptions.GeneralException;
import Trainings.TrainingDay;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import model.AlertDialog;
import sample.ShowTrainingDetail;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by jozef on 17.5.2017.
 * Controller to show window in which we load names of all trainings
 */
public class LoadController {
    @FXML
    private Button loadButton;
    @FXML
    private Button deleteButton;
    @FXML
    private ListView<TrainingDay> trainingDayListView;

    private Client client;

    /**
     * It is initialization of new window, it loads training data from database and fill list view with it,
     * throws an exception if a problem occurs.
     * @throws Exception
     */
    @FXML
    public void initialize() throws Exception {
        try {
            client = Client.getInstance();
            client.loadTrainings();
        }
        catch (GeneralException ex)
        {
            client.resetTrainings();
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
        trainingDayListView.setItems(client.getTrainingDays());
        if(!client.getTrainingDays().isEmpty())
            trainingDayListView.getSelectionModel().select(0);
        else {
            loadButton.setVisible(false);
            loadButton.setManaged(false);
        }
    }

    /**
     * It fills list view with data from database and shows window with training data,
     * throws an exception if a problem occurs.
     * @param actionEvent
     */
    public void loadHandler(ActionEvent actionEvent) {
        try{
            client.load(trainingDayListView.getSelectionModel().getSelectedItem().getName());
            Stage stage = (Stage)loadButton.getScene().getWindow();
            ShowTrainingDetail exerciseWindow = new ShowTrainingDetail(stage);
            client.resetTrainings();
            exerciseWindow.showAndWait();
        }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
        finally {
            client.resetTrainings();
        }
    }

    public void deleteHandler(ActionEvent actionEvent) {
        try {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Detele");
            alert.setContentText("Are you sure you want to delete this exercise?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                client.deleteTraining(trainingDayListView.getSelectionModel().getSelectedItem().getName());
                trainingDayListView.getItems().remove(trainingDayListView.getSelectionModel().getSelectedIndex());
                AlertDialog.success("Excercise was deleted successfully");
            }
        }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
    }
}
