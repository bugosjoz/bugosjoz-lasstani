package Controllers;

import Client.Client;
import Exceptions.GeneralException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.AlertDialog;
import sample.Registration;

import java.io.IOException;


/**
 * Created by stani on 23-Apr-17.
 * Controller to show login window
 */


public class LoginController{

    private Client client;

    @FXML
    private CheckBox checkBoxShow;
    @FXML
    private TextField loginUsername;
    @FXML
    private TextField loginTextPassword;
    @FXML
    private PasswordField passwordField;

    /**
     * The function called when the window is opened, it shows password when check box is checked.
     */
    @FXML
    public void initialize() {
        loginTextPassword.setManaged(false);
        loginTextPassword.setVisible(false);
        loginTextPassword.managedProperty().bind(checkBoxShow.selectedProperty());
        loginTextPassword.visibleProperty().bind(checkBoxShow.selectedProperty());
        passwordField.managedProperty().bind(checkBoxShow.selectedProperty().not());
        passwordField.visibleProperty().bind(checkBoxShow.selectedProperty().not());

        // Bind the textField and passwordField text values bidirectionally.
        loginTextPassword.textProperty().bindBidirectional(passwordField.textProperty());
    }

    /**
     * Click the hyperlink to enable the Registration window.
     * @param actionEvent
     */
    @FXML
    public void handleLinkAction(ActionEvent actionEvent) {
        Registration regWindow = new Registration(passwordField.getScene().getWindow());
        regWindow.showAndWait();
    }

    /**
     * When you click the button, it logs in to the user based on the entered data,
     * throws an exception if a problem occurs.
     * @param actionEvent
     */
    public void login(ActionEvent actionEvent) {
        try {
            client = Client.getInstance();
            client.logIn(loginUsername.getText(), passwordField.getText());
            Stage stage = (Stage) loginUsername.getScene().getWindow();
            AlertDialog.success("Login successful");
            stage.close();
        }
        catch (GeneralException  ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }

    }
}
