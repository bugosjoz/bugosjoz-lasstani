package Controllers;

import Client.Client;
import Exceptions.GeneralException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.AlertDialog;

import java.io.IOException;

/**
 * Created by jozef on 25.4.2017.
 * Controller to show window in which we can add new Exercise to database
 */
public class AddExerciseController {
    private Client client;

    @FXML
    private TextField nameTextField;
    @FXML
    private RadioButton timeRadioButton;
    @FXML
    private RadioButton distanceRadioButton;
    @FXML
    private RadioButton setsRadioButton;

    @FXML
    public void addExercise(ActionEvent actionEvent) {
        try {
            client = Client.getInstance();
            client.addExercise(nameTextField.getText(),timeRadioButton.isSelected(),distanceRadioButton.isSelected(),setsRadioButton.isSelected());
            AlertDialog.success("Excercise was added successfully");
            close(null);
        }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
    }

    /**
     * When you click the button close the Exercise Window,
     * throws an exception if a problem occurs.
     * @param actionEvent
     */
    @FXML
    public void close(ActionEvent actionEvent) {
        Stage stage = (Stage) nameTextField.getScene().getWindow();
        stage.close();
    }
}
