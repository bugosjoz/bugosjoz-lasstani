package Controllers;

import Client.Client;
import Exercises.Exercise;
import Exercises.ExerciseManager;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;

/**
 * Created by jozef on 26.4.2017.
 */
public class TestController {
    @FXML
    private ListView<Exercise> exerciseList;

    @FXML
    public void initialize() throws Exception {
        exerciseList.setItems(Client.getInstance().getExercises());
        if(!Client.getInstance().getExercises().isEmpty())
            exerciseList.getSelectionModel().select(0);
    }
}
