package Controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import sample.*;


/**
 * Controllet for main window
 */
public class Controller{
    @FXML
    private Label label1;

    /**
     * It shows window for adding new exercise.
     * @param actionEvent
     */
    @FXML
    private void addExercise(ActionEvent actionEvent) {
        NewTraining exerciseWindow = new NewTraining(label1.getScene().getWindow(),false);
        exerciseWindow.showAndWait();
    }

    /**
     * It shows window witch show all your exercises.
     * @param actionEvent
     */
    @FXML
    private void show(ActionEvent actionEvent) {
        ShowExercises showExercises = new ShowExercises(label1.getScene().getWindow());
        showExercises.showAndWait();
    }

    /**
     * It shows window for deleting exercise.
     * @param actionEvent
     */
    @FXML
    public void deleteExercise(ActionEvent actionEvent)  {
        DeleteExercise exerciseWindow = new DeleteExercise(label1.getScene().getWindow());
        exerciseWindow.showAndWait();
    }

    /**
     * It shows window with all users trainings.
     * @param actionEvent
     */
    public void load(ActionEvent actionEvent) {
        Load load = new Load(label1.getScene().getWindow());
        load.showAndWait();
    }
}
