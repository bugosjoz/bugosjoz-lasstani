package Controllers;

import Client.Client;
import Exceptions.GeneralException;
import Exceptions.UserException;
import Exercises.DistanceExercise;
import Exercises.Exercise;
import Exercises.SetsExercise;
import Exercises.TimeExercise;
import Trainings.Training;
import Trainings.TrainingDay;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import model.AlertDialog;
import model.Validator;
import sample.NewExercise;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

/**
 * Created by stani on 11-May-17.
 */
public class NewTrainingController {

    private Client client;
    private ObservableList<Training> items = FXCollections.observableArrayList();

    @FXML
    private DatePicker datePicker;
    @FXML
    private ChoiceBox<Exercise> exerciseChoiceBox;
    @FXML
    private ListView<Training> exerciseListView;
    @FXML
    private TextField textFieldTime;
    @FXML
    private TextField textFieldDistance;
    @FXML
    private TextField textFieldDistanceTime;
    @FXML
    private TextField textFieldSets;
    @FXML
    private TextField textFieldRepeats;
    @FXML
    private VBox timeBoxNewTrain;
    @FXML
    private HBox distanceBoxNewTrain;
    @FXML
    private  HBox setsBoxNewTrain;

    /**
     * The function called when the window is opened.
     * It changes fields by type of selected training in choice box or by type of selected training in list
     * and fills the fields,
     * throws an exception if a problem occurs.
     * @throws Exception
     */
    @FXML
    public void initialize() throws Exception{
        distanceBoxNewTrain.setManaged(false);
        distanceBoxNewTrain.setVisible(false);
        setsBoxNewTrain.setManaged(false);
        setsBoxNewTrain.setVisible(false);
        addTextListeners();

        client = Client.getInstance();
        client.resetTrainings();
        items.clear();
        exerciseListView.setItems(client.getTrainings());
        exerciseChoiceBox.setItems(client.getExercises());

        //by type of selected training in choice box
        exerciseChoiceBox.getSelectionModel().selectedItemProperty().addListener((selected, oldExercise, newExercise) -> {
            if(newExercise instanceof SetsExercise)
                isSets();
            else if(newExercise instanceof TimeExercise)
                isTime();
            else if(newExercise instanceof DistanceExercise)
                isDistance();
            else {
                AlertDialog.alert("Error training");
            }
            eraseFields();
        });

        //by type of selected training in list
        exerciseListView.getSelectionModel().selectedItemProperty().addListener((selected, oldTraining, newTraining) -> {
            if(newTraining == null)
                return;
            Exercise exercise = newTraining.getCurrentExercise();
            if(exercise == null)
                return;
            eraseFields();
            int index = 0;
            boolean isSelected = false;
            for (Exercise exercise1 : exerciseChoiceBox.getItems())
            {
                if(exercise1.getName().equals(exercise.getName()))
                {
                    isSelected = true;
                    exerciseChoiceBox.getSelectionModel().select(index);
                    break;
                }
                index++;
            }
            if(!isSelected)
            {
                AlertDialog.alert("Error training");
            }
            else
            if(exercise instanceof SetsExercise)
                setsBind((SetsExercise)exercise);
            else if(exercise instanceof TimeExercise)
                timeBind((TimeExercise)exercise);
            else if(exercise instanceof DistanceExercise)
                distanceBind((DistanceExercise)exercise);
            else {
                AlertDialog.alert("Error training");
            }
        });
        if(!client.getExercises().isEmpty())
            exerciseChoiceBox.getSelectionModel().select(0);
        if(!client.getExercises().isEmpty())
            exerciseListView.getSelectionModel().select(0);
    }

    /**
     * Listeners for validation of text fields.
     */
    private void addTextListeners()
    {
        textFieldDistance.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("") && (!newValue.matches("[0-9]+") || Integer.parseInt(newValue) == 0 )) {
                textFieldDistance.setText(oldValue);
                AlertDialog.alert("Wrong value");
            }
        });
        textFieldDistanceTime.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("") && (!newValue.matches("[0-9]+") || Integer.parseInt(newValue) == 0 )) {
                textFieldDistanceTime.setText(oldValue);
                AlertDialog.alert("Wrong value");
            }
        });
        textFieldRepeats.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("") && (!newValue.matches("[0-9]+") || Integer.parseInt(newValue) == 0 )) {
                textFieldRepeats.setText(oldValue);
                AlertDialog.alert("Wrong value");
            }
        });
        textFieldSets.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("") && (!newValue.matches("[0-9]+") || Integer.parseInt(newValue) == 0 )) {
                textFieldSets.setText(oldValue);
                AlertDialog.alert("Wrong value");
            }
        });
        textFieldTime.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("") && (!newValue.matches("[0-9]+") || Integer.parseInt(newValue) == 0 )) {
                textFieldTime.setText(oldValue);
                AlertDialog.alert("Wrong value");
            }
        });
    }

    /**
     * It sets field text by selected exercise.
     * @param exercise
     */
    private void distanceBind(DistanceExercise exercise) {
        textFieldDistanceTime.setText(String.valueOf(exercise.getMinutes()));
        textFieldDistance.setText(String.valueOf(exercise.getDistance()));
    }

    /**
     * It sets field text by selected exercise.
     * @param exercise
     */
    private void timeBind(TimeExercise exercise) {
        textFieldTime.setText(String.valueOf(exercise.getMinutes()));
    }

    /**
     * It sets field text by selected exercise.
     * @param exercise
     */
    private void setsBind(SetsExercise exercise) {
        textFieldSets.setText(String.valueOf(exercise.getSets()));
        textFieldRepeats.setText(String.valueOf(exercise.getReps()));
    }

    /**
     * Click the hyperlink to enable the window for new training.
     * @param actionEvent
     */
    public void handleLinkAction(ActionEvent actionEvent) {
        NewExercise regWindow = new NewExercise(timeBoxNewTrain.getScene().getWindow());
        regWindow.showAndWait();
    }

    /**
     * It shows just fields for time exercise.
     */
    private void isTime() {
        distanceBoxNewTrain.setManaged(false);
        distanceBoxNewTrain.setVisible(false);
        setsBoxNewTrain.setManaged(false);
        setsBoxNewTrain.setVisible(false);
        timeBoxNewTrain.setManaged(true);
        timeBoxNewTrain.setVisible(true);
    }

    /**
     * It shows just fields for distance exercise.
     */
    private void isDistance() {
        distanceBoxNewTrain.setManaged(true);
        distanceBoxNewTrain.setVisible(true);
        setsBoxNewTrain.setManaged(false);
        setsBoxNewTrain.setVisible(false);
        timeBoxNewTrain.setManaged(false);
        timeBoxNewTrain.setVisible(false);
    }

    /**
     * It shows just fields for sets exercise.
     */
    private void isSets() {
        distanceBoxNewTrain.setManaged(false);
        distanceBoxNewTrain.setVisible(false);
        setsBoxNewTrain.setManaged(true);
        setsBoxNewTrain.setVisible(true);
        timeBoxNewTrain.setManaged(false);
        timeBoxNewTrain.setVisible(false);
    }

    private int tryParse(String s)
    {
        int num;
        try {
            num = Integer.parseInt(s);
        }
        catch (NumberFormatException ex)
        {
            num = 0;
        }
        return num;
    }

    public void handleCancel(ActionEvent actionEvent) {
        Stage stage = (Stage) timeBoxNewTrain.getScene().getWindow();
        stage.close();
    }

    /**
     * When you click the button it adds the entered data to the list.
     * @param actionEvent
     */
    public void addButton(ActionEvent actionEvent) {
        Exercise exercise = exerciseChoiceBox.getValue();
        //Validation for sets exercise.
        if(exercise instanceof SetsExercise) {
            if(textFieldSets.getText().equals("") || textFieldRepeats.getText().equals(""))
            {
                AlertDialog.alert("Wrong value");
                return;
            }
            items.add(new Training(new SetsExercise(exercise.getName(), tryParse(textFieldSets.getText()), tryParse(textFieldRepeats.getText()))));
        }
        //Validation for time exercise.
        else if(exercise instanceof TimeExercise) {
            if(textFieldTime.getText().equals(""))
            {
                AlertDialog.alert("Wrong value");
                return;
            }
            items.add(new Training(new TimeExercise(exercise.getName(),tryParse(textFieldTime.getText()))));
        }
        //Validation for distance exercise.
        else if(exercise instanceof DistanceExercise) {
            if(textFieldDistanceTime.getText().equals("") || textFieldDistance.getText().equals(""))
            {
                AlertDialog.alert("Wrong value");
                return;
            }
            items.add(new Training(new DistanceExercise(exercise.getName(), tryParse(textFieldDistance.getText()), tryParse(textFieldDistanceTime.getText()))));
        }
        else {
            AlertDialog.alert("Error training");
        }
        eraseFields();
        exerciseListView.setItems(items);
    }

    private void eraseFields() {
        textFieldDistance.setText("");
        textFieldDistanceTime.setText("");
        textFieldTime.setText("");
        textFieldRepeats.setText("");
        textFieldSets.setText("");
    }

    /**
     * When you click the button it removes the marked data from the list.
     * @param actionEvent
     */
    public void removeButton(ActionEvent actionEvent) {
        //Modal dialog for confirmation of remove.
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Detele");
        alert.setContentText("Are you sure you want to delete this exercise?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent()) {
            if (result.get() == ButtonType.OK) {
                items.remove(exerciseListView.getSelectionModel().getSelectedItem());
            }
        }
    }

    /**
     * It validates training field and if its alright it saves data to database,
     * throws an exception if a problem occurs.
     * @param actionEvent
     */
    public void saveTraining(ActionEvent actionEvent) {
        try {
            if(items.isEmpty())
                throw new GeneralException("No trainings to save.");
            try {
                Validator.validateDate(datePicker.getValue());
            }
            catch(UserException ex)
            {
                throw new GeneralException(ex.getMessage());
            }
            TextInputDialog dialog = new TextInputDialog("Training");
            dialog.setTitle("Save training");
            dialog.setContentText("Save as:");
            Optional<String> result = dialog.showAndWait();
            if (result.isPresent()) {
                client.save(result.get(), datePicker.getValue(),items);
                handleCancel(null);
            }
        }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
    }

}
