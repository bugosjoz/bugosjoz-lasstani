package Controllers;

import Client.Client;
import Exceptions.GeneralException;
import Exercises.Exercise;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.stage.Stage;
import model.AlertDialog;

import java.io.IOException;
import java.util.Optional;

/**
 * Created by jozef on 2.5.2017.
 * Controller to show window in which we can delete new Exercise to database
 */
public class DeleteExerciseController {

    private Client client;

    @FXML
    private ListView<Exercise> exerciseList;

    /**
     * The function called when the window is opened.
     * @throws Exception
     */
    @FXML
    public void initialize() throws Exception {
        client = Client.getInstance();
        exerciseList.setItems(client.getExercises());
        if(!Client.getInstance().getExercises().isEmpty())
            exerciseList.getSelectionModel().select(0);
    }

    /**
     * When you click the button it clears the training according to the entered data,
     * throws an exception if a problem occurs.
     * @param actionEvent
     */
    @FXML
    public void deleteExercise(ActionEvent actionEvent)  {
        try {
            if(exerciseList.getItems().size() == 1)
                throw new GeneralException("Last exercise can not be deleted!");
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Detele");
            alert.setContentText("Are you sure you want to delete this exercise?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.get() == ButtonType.OK){
                client.deleteExercise(exerciseList.getSelectionModel().getSelectedItem().getName(),exerciseList.getSelectionModel().getSelectedIndex());
                AlertDialog.success("Excercise was deleted successfully");
                close(null);
            }
        }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
    }

    /**
     * When you click the button window will be closed.
     * @param actionEvent
     */
    @FXML
    public void close(ActionEvent actionEvent) {
        Stage stage = (Stage) exerciseList.getScene().getWindow();
        stage.close();
    }
}
