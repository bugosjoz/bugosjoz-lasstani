package Controllers;

import Client.Client;
import Exceptions.GeneralException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import model.AlertDialog;

import java.io.IOException;

/**
 * Created by stani on 24-Apr-17.
 * Controller to show registration window
 */
public class RegistrationController {

    private Client client;

    @FXML
    private CheckBox confirmCheckBoxShow;
    @FXML
    private TextField confirmTextField;
    @FXML
    private CheckBox agreeCheckBox;
    @FXML
    private TextField nameTextField;
    @FXML
    private TextField surnameTextField;
    @FXML
    private TextField usernameTextField;
    @FXML
    private TextField mailTextField;
    @FXML
    private PasswordField confirmPasswordField;
    @FXML
    private DatePicker birthDatePicker;
    @FXML
    private RadioButton maleCheckBox;
    @FXML
    private TextField regTextPassword;
    @FXML
    private PasswordField regPasswordField;
    @FXML
    private CheckBox regCheckBoxShow;
    @FXML
    private Label regLabelWeight;
    @FXML
    private Label regLabelHeight;
    @FXML
    private Slider regSliderWeight;
    @FXML
    private Slider regSliderHeight;
    @FXML
    private Button closeButton;

    /**
     * The function called when the window is opened,
     * when is the choice box checked it will show a password and changing values when slider is changed.
     */
    @FXML
    public void initialize() {
        regTextPassword.setManaged(false);
        regTextPassword.setVisible(false);
        regTextPassword.managedProperty().bind(regCheckBoxShow.selectedProperty());
        regTextPassword.visibleProperty().bind(regCheckBoxShow.selectedProperty());
        regPasswordField.managedProperty().bind(regCheckBoxShow.selectedProperty().not());
        regPasswordField.visibleProperty().bind(regCheckBoxShow.selectedProperty().not());

        // Bind the textField and passwordField text values bidirectionally.
        regTextPassword.textProperty().bindBidirectional(regPasswordField.textProperty());

        confirmTextField.setManaged(false);
        confirmTextField.setVisible(false);
        confirmTextField.managedProperty().bind(confirmCheckBoxShow.selectedProperty());
        confirmTextField.visibleProperty().bind(confirmCheckBoxShow.selectedProperty());
        confirmPasswordField.managedProperty().bind(confirmCheckBoxShow.selectedProperty().not());
        confirmPasswordField.visibleProperty().bind(confirmCheckBoxShow.selectedProperty().not());

        // Bind the textField and passwordField text values bidirectionally.
        confirmTextField.textProperty().bindBidirectional(confirmPasswordField.textProperty());

        regSliderWeight.valueProperty().addListener((observable, oldValue, newValue) -> {
            String value = String.format("%.2fkg", newValue.doubleValue());
            regLabelWeight.setText(value);
        });

        regSliderHeight.valueProperty().addListener((observable, oldValue, newValue) -> {
            String value = String.format("%.2fcm", newValue.doubleValue());
            regLabelHeight.setText(value);
        });
    }

    /**
     * When you click the button it registers the user based on the entered data,
     * throws an exception if a problem occurs.
     * @param actionEvent
     */
    public void register(ActionEvent actionEvent) {
        try {
            client = Client.getInstance();
            client.doRegistration(nameTextField.getText(),surnameTextField.getText(),usernameTextField.getText(),
                                    mailTextField.getText(),regTextPassword.getText(),confirmPasswordField.getText(),
                                    birthDatePicker.getValue(),maleCheckBox.isSelected(),regSliderHeight.getValue(),regSliderWeight.getValue(),agreeCheckBox.isSelected());
            AlertDialog.success("Registration successful");
            closeHandleAction(null);
        }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
        }
        catch (IOException ex)
        {
            AlertDialog.alert("Error occurred during communication with server.");
        }
    }

    public void closeHandleAction(ActionEvent actionEvent) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }
}
