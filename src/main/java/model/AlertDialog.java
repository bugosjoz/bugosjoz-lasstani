package model;

import javafx.scene.control.Alert;

/**
 * Created by jozef on 24.4.2017.
 * Static class with methods to create DialogWindows
 */
public class AlertDialog {

    private AlertDialog()
    {}
    /**
     * It creates new dialog window.
     * @param message
     * @param type
     */
    private static void dialog(String message, Alert.AlertType type)
    {
        Alert alert = new Alert(type);
        alert.setHeaderText(null);
        alert.setTitle("Error Dialog");
        alert.setContentText(message);

        alert.showAndWait();
    }
    public static void alert(String message)
    {
        dialog(message, Alert.AlertType.ERROR);
    }
    public static void success(String message)
    {
        dialog(message, Alert.AlertType.INFORMATION);
    }

}
