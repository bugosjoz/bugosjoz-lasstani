package model;

import Exceptions.UserException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.time.LocalDate;

/**
 * Created by jozef on 25.5.2017.
 * Static class to validate data
 */
public class Validator {

    public Validator(){}

    /**
            * It validates a password of at least 6 and a maximum of 64 characters,
            * both passwords must match if the second password is entered,
            * throws an exception if a problem occurs.
     * @param password String
     * @param password_again String
     * @throws UserException
     */
    static public boolean validatePassword(String password, String password_again) throws UserException {

        // ak nie je heslo zadané
        if ("".equals(password)) {
            throw new UserException("The password field is empty.");
        }

        // ak je heslo príliš krátke
        if (password.length() < 6) {
            throw new UserException("The password must be long at least 6 characters.");
        }

        if (password.length() > 64) {
            throw new UserException("The password must be long less than 64 characters.");
        }

        // ak máme aj kontrolné heslo
        if (null != password_again) {

            // ak je kontrolné heslo prázdne
            if ("".equals(password_again)) {
                throw new UserException("The password again field is empty.");
            }

            // ak sa heslá nezhodujú
            if (!password.equals(password_again)) {
                throw new UserException("Passwords do not match.");
            }
        }
        return true;
    }

    /**
     * Validates username, min 3 and max 64 characters, only numbers and letters,
     * throws an exception if a problem occurs.
     * @param name String
     * @throws UserException
     */
    static public boolean validateUserName(String name) throws UserException {

        // ak meno nie je zadané
        if ("" == name) {
            throw new UserException("The name field is empty.");
        }

        // ak je meno príliš krátne
        if (name.length() < 3) {
            throw new UserException("The name must be long at least 3 characters.");
        }

        // ak je meno príliš dlhé
        if (name.length() > 64) {
            throw new UserException("The name must be long less than 64 characters.");
        }
        String pattern= "^[a-zA-Z0-9]*$";
        if(!(name.matches(pattern)))
            throw new UserException("The name contains unauthorized characters.");
        return true;
    }


    /**
     * Validates email,
     * throws an exception if a problem occurs
     * @param email
     * @throws UserException
     */
    static public boolean validateMail(String email) throws UserException {
        if ("".equals(email)) {
            throw new UserException("The email field is empty.");
        }
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            throw new UserException("Wrong email address");
        }
        return true;
    }

    /**
     * It validates name, only characters are available,
     * Throws an exception if a problem occurs
     * @param name
     * @throws UserException
     */
    static public boolean validateName(String name) throws UserException {
        if(!name.matches("[a-zA-Z]+"))
            throw new UserException("Wrong name!");
        return true;
    }

    /**
     * Validates the date when the future will throw away the exception.
     * @param date
     * @throws UserException
     */
    static public boolean validateDate(LocalDate date) throws UserException
    {
        if(date == null)
            throw new UserException("Choose date");
        if(date.isAfter(LocalDate.now()))
            throw new UserException("Date can't be in the future");
        return true;
    }

    /**
     * @param x
     * @param name
     * @throws UserException
     */
    static public boolean validateDouble(double x, String name) throws UserException
    {
        if(x < 0)
            throw new UserException("Must be greater than zero");
        if(x == 0.0)
            throw new UserException("Choose "+ name);
        return true;
    }
}
