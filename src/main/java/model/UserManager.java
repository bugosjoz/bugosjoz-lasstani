package model;

import DB.Database;
import Exceptions.UserException;
import javafx.scene.control.Alert;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by jozef on 24.4.2017.
 * Class which work with users and their data.
 * Registrate, login, save to database.
 * Adding 2 random salts to passwords
 */
public class UserManager {
    private Database db;

    public UserManager() throws SQLException
    {
            db = Database.getInstance();
            classLock = true;
    }
    public UserManager(Database db) throws SQLException
    {
        if(db == null)
            new UserManager();
        else {
            this.db = db;
            classLock = true;
        }
    }

    private static boolean classLock = false;

    private static String secretPasswordSalt = "";

    /**
     * Sets secret salt for password.
     * throws an exception if a problem occurs.
     * @param  string String
     * @throws IllegalArgumentException
     */
    public static void setSecretPasswordSalt(String string) {
        if (classLock) {
            throw new IllegalArgumentException("You cannot change salt");
        }
        secretPasswordSalt = string;
    }

    /**
     * Returns randomly generated 128-character salt.
     * @return string
     */
    private String generateSalt() throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        return (new HexBinaryAdapter()).marshal(md.digest(bytes));
    }

    /**
     * Returns a 128-character hash combination of password, salt, and private key.
     * The method can not be changed after production.
     * @param password String
     * @param salt String
     * @return string
     */
    private String getPasswordHash(String password, String salt) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        String pass = password + salt;
        return (new HexBinaryAdapter()).marshal(md.digest(pass.getBytes()));
    }



    /**
     * Checks the login data and then log in user,
     * throws an exception if a problem occurs.
     * @param name
     * @param password
     * @return index
     * @throws UserException
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     */
    public int doLogin(String name, String password) throws UserException, SQLException,NoSuchAlgorithmException
    {
        Validator.validateName(name);
        Object[] col = {"salt"};
        Object[] params = {name};
        ResultSet rs = db.select("users",col,"username = ?", params);
        if (!rs.isBeforeFirst() )
           throw new UserException("Wrong name");
        rs.next();
        String salt = rs.getString("salt");
        Validator.validatePassword(password,null);
        Object[] col1 = {"users_id"};
        Object[] pamars1 = {name, getPasswordHash(password,salt)};
        rs = db.select("users",col1,"username = ? AND password = ?",pamars1);
        if (!rs.isBeforeFirst() )
            throw new UserException("Wrong password");
        rs.next();
        return rs.getInt("users_id");
    }

    /**
     * Validates the entered data, verifies passwords and creates a record in the database,
     * throws an exception if a problem occurs.
     * @param name
     * @param surname
     * @param username
     * @param mail
     * @param password
     * @param passwordAgain
     * @param birthday
     * @param male
     * @param height
     * @param weight
     * @param agree
     * @throws UserException
     * @throws SQLException
     * @throws NoSuchAlgorithmException
     */
    public void doRegistration(String name, String surname, String username, String mail, String password, String passwordAgain, LocalDate birthday, boolean male, double height, double weight,boolean agree) throws UserException, SQLException,NoSuchAlgorithmException
    {
        if(!agree)
            throw new UserException("You have to agree with our conditions!");
        Validator.validateUserName(username);
        Validator.validateName(surname);
        Validator.validateName(name);
        if("".equals(passwordAgain))
            throw new UserException("Please enter confirmation password.");
        Validator.validatePassword(password,passwordAgain);
        Validator.validateMail(mail);
        Validator.validateDate(birthday);
        Validator.validateDouble(weight,"weight");
        Validator.validateDouble(height,"height");
        String salt = generateSalt();
        password = getPasswordHash(password,salt);
        String[] columns = {"name","surname","username","mail","password","salt", "male", "birthday","height","weight"};
        Object[] params = {name,surname,username,mail,password,salt,male,birthday,height,weight};
        try {db.insert("users",columns,params);}
        catch (SQLException ex)
        {   System.out.println(ex.getMessage());
            throw new UserException("Username already exists.");}
    }
}

