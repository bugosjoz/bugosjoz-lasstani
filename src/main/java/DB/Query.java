
package DB;

/**
 * Created by jozef on 24.4.2017.
 */
public class Query {
    private StringBuilder query;

    public String getQuery() {
        return query.toString();
    }

    /**
     *
     * @param table
     * @return
     */
    public Query from(String table){
        query.append(" FROM ");
        query.append(table);
        return this;
    }

    public Query delete(String table)
    {
        query = new StringBuilder();
        query.append("DELETE FROM ");
        query.append(table);
        return this;
    }

    public Query where(String requirement)
    {
        query.append(" WHERE ");
        query.append(requirement);
        return this;
    }

    /**
     *
     * @param table
     * @return
     */
    public Query update(String table){
        query = new StringBuilder();
        query.append("UPDATE ");
        query.append(table);
        query.append(" SET ");
        return this;
    }

    /**
     * It adds columns.
     * @param columns
     */
    public Query set(String[] columns){
        int count = columns.length;
        if(count == 0)
            throw new IllegalArgumentException("Illegal column's count!");
        for(String column : columns)
        {
            query.append(column);
            query.append(" = ?,");
        }
        query.deleteCharAt(query.lastIndexOf(","));
        return this;
    }

    /**
     *
     * @param table
     * @return
     */
    public Query insert(String table){
        query = new StringBuilder();
        query.append("INSERT INTO ");
        query.append(table);
        return this;
    }

    public Query insert(String table,String[] into){
        query = new StringBuilder();
        query.append("INSERT INTO ");
        query.append(table);
        query.append(" (");
        for(String one : into)
        {
            query.append(one);
            query.append(",");
        }
        query.deleteCharAt(query.lastIndexOf(","));
        query.append(")");
        return this;
    }

    /**
     *
     * @param params
     * @return
     */
    public Query values(Object[] params)
    {
        query.append(" VALUES(");
        int count = params.length;
        if(count == 0)
            throw new IllegalArgumentException("Illegal number of parameters");
        for(int i = 0; i < count; i++)
        {
            query.append("?,");
        }
        query.deleteCharAt(query.lastIndexOf(","));
        query.append(");");
        return this;
    }

    /**
     *
     * @param columns
     * @return
     */
    public Query select(Object[] columns){
        query = new StringBuilder();
        query.append("SELECT ");
        if(columns != null)
        {
            for(Object obj: columns)
            {
                query.append(obj);
                query.append(",");
            }
            query.deleteCharAt(query.lastIndexOf(","));
        }
        else
            query.append("*");
        return this;
    }

}
