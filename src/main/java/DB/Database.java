package DB;

import model.AlertDialog;

import java.sql.*;

/**
 * Created by jozef on 24.4.2017.
 * Thread-safe singleton
 * Wrapper around Java DriverManager
 * with methods to work with SQL database
 */
public class Database {
    private static Connection connection;
    private static Database instance = null;
    private static final Object mutex = new Object();
    protected Query query;

    /**
     * Static method for returning a database instance.
     * @return Database
     */
    public static Database getInstance() throws SQLException {
        if (null == instance) {
            synchronized (mutex){
                if(instance==null)
                    instance = new Database("pjv","root","");
            }
        }
        return instance;
    }

    /**
     * Connection to the database.
     * @param db
     * @param userName
     * @param password
     * @throws SQLException
     */
    private Database(String db, String userName, String password) throws SQLException
    {
        connection = DriverManager.getConnection("jdbc:mysql://localhost/"+db, userName, password);
    }

    /**
     * Execute query with preparedStatements
     *
     * @param query
     * @param params
     * @return preparedStatemant
     * @throws SQLException
     */
    private int query(String query, Object[] params) throws SQLException{
        PreparedStatement ps = connection.prepareStatement(query);
        if (params != null){
            int index = 1;
            for(Object param : params){
                ps.setObject(index, param);
                index++;
            }
        }
        return ps.executeUpdate();
    }

    /**
     * It deletes data form database.
     * @param table
     * @param requirement
     * @param param
     * @return query
     * @throws SQLException
     */
    public int delete(String table, String requirement, Object[] param) throws SQLException{
        query = new Query();
        query.delete(table).where(requirement);
        return query(query.getQuery(), param);
    }

    /**
     * It saves to the database a subject.
     * @param table
     * @param params
     * @return query
     * @throws java.sql.SQLException
     */
    public int insert(String table, String[] into, Object[] params) throws SQLException{
        query = new Query();
        query.insert(table,into).values(params);
        return query(query.getQuery(), params);
    }

    /**
     * It returns data from database.
     * @param table
     * @param columns
     * @param params
     * @return
     * @throws SQLException
     */
    public ResultSet select(String table, Object[] columns, Object[] params) throws SQLException{
        return this.select(table, columns, "", params);
    }

    /**
     * It returns data from database.
     * @param table
     * @param columns
     * @param requirement
     * @param params
     * @return result
     * @throws SQLException
     */
    public ResultSet select(String table, Object[] columns, String requirement, Object[] params) throws SQLException{
        query = new Query();
        if(requirement.equals(""))
            query.select(columns).from(table);
        else
            query.select(columns).from(table).where(requirement);
        PreparedStatement ps = connection.prepareStatement(query.getQuery());
        if(params != null){
            int index = 1;
            for(Object param : params){
                ps.setObject(index, param);
                index++;
            }
        }

        return ps.executeQuery();
    }
    /**
     * Get counts from database
     *
     * @param table
     * @return
     * @throws SQLException
     */
    public int count(String table) throws SQLException{
        PreparedStatement ps =
                connection.prepareStatement("SELECT COUNT(*) FROM "+table);
        ResultSet result = ps.executeQuery();
        result.next();
        return result.getInt(1);
    }
}

