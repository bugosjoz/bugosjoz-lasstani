package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 * Created by jozef on 17.5.2017.
 */
public class Load extends Stage {
    public Load(Window window)
    {
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("load.fxml"));
            setTitle("Load");
            setWidth(400);
            setHeight(400);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.WINDOW_MODAL);
            initOwner(window);
            setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}