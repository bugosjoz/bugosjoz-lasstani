package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 * Created by jozef on 26.4.2017.
 */
public class NewTraining extends Stage {
    public NewTraining(Window window, boolean hide){
        try {
            setWidth(700);
            setHeight(500);
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("newTraining.fxml"));
            setTitle("New Training");
            initStyle(StageStyle.UTILITY);
            initModality(Modality.WINDOW_MODAL);
            setResizable(false);
            initOwner(window);
            setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
