package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 * Created by stani on 11-May-17.
 */
public class NewExercise extends Stage {
    public NewExercise(Window window){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("addExercise.fxml"));
            setTitle("New Exercise");
            setWidth(500);
            setHeight(350);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.WINDOW_MODAL);
            setResizable(false);
            initOwner(window);
            setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
