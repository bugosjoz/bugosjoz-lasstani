package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 * Created by jozef on 26.4.2017.
 */
public class ShowExercises extends Stage {
    public ShowExercises(Window window)
    {
        try {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("showExercises.fxml"));
        setTitle("Exercise");
        setWidth(800);
        setHeight(400);
        initStyle(StageStyle.UTILITY);
        initModality(Modality.WINDOW_MODAL);
        initOwner(window);
        setScene(new Scene(root));
    }
        catch (IOException e) {
        e.printStackTrace();
    }
}
}
