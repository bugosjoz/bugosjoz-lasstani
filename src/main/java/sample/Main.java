package sample;

import Client.Client;
import Exceptions.GeneralException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import model.AlertDialog;
import model.UserManager;

import java.io.IOException;
import java.sql.SQLException;

public class Main extends Application {
    ///TODO kontorly na NULL pri predávaní na server?
    @Override
    public void start(Stage primaryStage) throws Exception{
        boolean isLoggedIn = false;
        Client client;
        try  {client = Client.getInstance();
        Login loginWindow = new Login();
        loginWindow.showAndWait();
        isLoggedIn = client.isLoggedIn(); }
        catch (GeneralException ex)
        {
            AlertDialog.alert(ex.getMessage());
            System.exit(1);
        }
        catch (Exception ex)
        {
            AlertDialog.alert("Server not responding");
            System.exit(1);
        }
        if(!isLoggedIn)
            System.exit(1);
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("mainPage.fxml"));
        primaryStage.setTitle("Hello World");
        Scene scene = new Scene(root, 700, 500);
        //scene.getStylesheets().add(getClass().getResource("/style.css").toExternalForm());
        primaryStage.setScene(scene);
        Client.getInstance();
        primaryStage.show();
    }


    public static void main(String[] args) {
        UserManager.setSecretPasswordSalt("hlkôajsdkf3215dfa&#asdf"); launch(args);
    }
}
