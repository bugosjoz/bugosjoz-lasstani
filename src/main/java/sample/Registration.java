package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

import static javafx.application.Application.launch;

/**
 * Created by stani on 24-Apr-17.
 */
public class Registration extends Stage{

    public Registration(Window window){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("registrationPage.fxml"));
            setTitle("Registration");
            setWidth(400);
            setHeight(700);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.WINDOW_MODAL);
            initOwner(window);
            setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
