package sample;

import Client.Client;
import Exceptions.GeneralException;
import Exercises.DistanceExercise;
import Exercises.Exercise;
import Exercises.SetsExercise;
import Exercises.TimeExercise;
import Trainings.Training;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import model.AlertDialog;

import java.io.IOException;

/**
 * Created by stani on 23-May-17.
 */
public class ShowTrainingDetail extends Stage{

    private DatePicker datePicker;
    private TextField distanceTimeText;
    private TextField distanceText;
    private TextField timeText;
    private TextField sets;
    private TextField repeats;
    private HBox hBoxSets;
    private HBox hBoxDistance;
    private VBox vBoxTime;
    private Label labeExercise;

    /**
     * It create new modal window with dimensions 700x400,
     * throws an exception if a problem with client occurs.
     * @param window
     * @throws IOException
     * @throws GeneralException
     */
    public ShowTrainingDetail(Window window) throws IOException, GeneralException {
        setWidth(700);
        setHeight(400);
        setTitle("Loaded Training");
        initStyle(StageStyle.UTILITY);
        initModality(Modality.WINDOW_MODAL);
        setResizable(false);
        initOwner(window);
        setScene(trainingDetails());
    }

    /**
     * It sets up new window consist of widgets to show training details,
     * throws an exception if a problem with client occurs.
     * @return Returns new scene for training details
     * @throws IOException
     * @throws GeneralException
     */
    private Scene trainingDetails() throws IOException, GeneralException {
        //Get instance of Client
        Client client = Client.getInstance();

        //Create new VBox witch is anchored to anchor pane
        VBox vboxDetails = new VBox(10);
        vboxDetails.setAlignment(Pos.CENTER);
        AnchorPane.setBottomAnchor(vboxDetails, 0.0);
        AnchorPane.setTopAnchor(vboxDetails, 0.0);
        AnchorPane.setRightAnchor(vboxDetails, 0.0);
        AnchorPane.setLeftAnchor(vboxDetails, 0.0);

        //Create new Label for title with bolt font and size 24
        Label title = new Label("Training");
        title.setAlignment(Pos.CENTER);
        title.setFont(Font.font("System Bold", FontWeight.BOLD, 24));

        Label dateOfTraining = new Label("Date of Training:");
        dateOfTraining.setAlignment(Pos.CENTER);

        //Create new DatePicker with date of training and it is not editable
        datePicker = new DatePicker(client.getTrainingDate());
        datePicker.setEditable(false);
        datePicker.setDisable(true);
        datePicker.setStyle("-fx-opacity: 1");
        datePicker.getEditor().setStyle("-fx-opacity: 1");

        HBox hBox = new HBox(10);
        hBox.setAlignment(Pos.CENTER);
        hBox.setFillHeight(true);

        VBox vBoxInfo = new VBox(10);
        vBoxInfo.setAlignment(Pos.CENTER);
        vBoxInfo.setFillWidth(true);

        //Create new Label for name of training with font size 24
        labeExercise = new Label();
        labeExercise.setAlignment(Pos.CENTER);
        labeExercise.setFont(Font.font("System Bold", 24));

        vBoxTime = new VBox(10);
        vBoxTime.setAlignment(Pos.CENTER);

        Label timeTime = new Label("Time (min)");
        timeTime.setAlignment(Pos.CENTER);

        timeText = new TextField();
        timeText.setEditable(false);

        hBoxDistance = new HBox(10);
        hBoxDistance.setAlignment(Pos.CENTER);

        VBox vBoxDistance = new VBox(10);
        vBoxDistance.setAlignment(Pos.CENTER);

        Label distanceDistance = new Label("Distance:");
        distanceDistance.setAlignment(Pos.CENTER);

        distanceText = new TextField();
        distanceText.setEditable(false);

        VBox vBoxDistanceTime = new VBox(10);
        vBoxDistanceTime.setAlignment(Pos.CENTER);

        Label distanceTime = new Label("Time:");
        distanceTime.setAlignment(Pos.CENTER);

        distanceTimeText = new TextField();
        distanceTimeText.setEditable(false);

        hBoxSets = new HBox(10);
        hBox.setAlignment(Pos.CENTER);

        VBox vBoxSets = new VBox(10);
        vBoxSets.setAlignment(Pos.CENTER);

        Label setsSets = new Label("Sets:");
        setsSets.setAlignment(Pos.CENTER);

        sets = new TextField();
        sets.setEditable(false);

        VBox vBoxRepeats = new VBox(10);
        vBoxRepeats.setAlignment(Pos.CENTER);

        Label setsRepeats = new Label("Repeats:");
        setsRepeats.setAlignment(Pos.CENTER);

        repeats = new TextField();
        repeats.setEditable(false);

        ListView<Training> listView = new ListView<>();
        listView.setMaxHeight(200);

        //Assign all children to their perents
        vBoxRepeats.getChildren().addAll(setsRepeats, repeats);
        vBoxSets.getChildren().addAll(setsSets, sets);
        hBoxSets.getChildren().addAll(vBoxSets, vBoxRepeats);
        vBoxDistanceTime.getChildren().addAll(distanceTime, distanceTimeText);
        vBoxDistance.getChildren().addAll(distanceDistance, distanceText);
        hBoxDistance.getChildren().addAll(vBoxDistance, vBoxDistanceTime);
        vBoxTime.getChildren().addAll(timeTime, timeText);
        vBoxInfo.getChildren().addAll(labeExercise, vBoxTime, hBoxDistance, hBoxSets);
        hBox.getChildren().addAll(vBoxInfo, listView);
        vboxDetails.getChildren().addAll(title, dateOfTraining, datePicker, hBox);

        //Set all fields for trainings off
        hBoxDistance.setManaged(false);
        hBoxDistance.setVisible(false);
        hBoxSets.setManaged(false);
        hBoxSets.setVisible(false);
        vBoxTime.setManaged(false);
        vBoxTime.setVisible(false);

        //Fill listView with training data and choose witch fields will be showed by selected exercise
        listView.setItems(client.getTrainings());
        listView.getSelectionModel().selectedItemProperty().addListener((selected, oldTraining, newTraining) -> {
            if(newTraining == null)
                return;
            Exercise exercise = newTraining.getCurrentExercise();
            if(exercise == null)
                return;
            labeExercise.setText(exercise.getName());
            if(exercise instanceof SetsExercise) {
                isSets();
                setsBind((SetsExercise) exercise);
            }
            else if(exercise instanceof TimeExercise) {
                isTime();
                timeBind((TimeExercise) exercise);
            }
            else if(exercise instanceof DistanceExercise) {
                isDistance();
                distanceBind((DistanceExercise) exercise);
            }
            else {
                AlertDialog.alert("Error training");
            }
        });

        return new Scene(vboxDetails);
    }

    /**
     * It fills fields for distance training with training values.
     * @param exercise
     */
    private void distanceBind(DistanceExercise exercise) {
        distanceTimeText.setText(String.valueOf(exercise.getMinutes()));
        distanceText.setText(String.valueOf(exercise.getDistance()));
    }

    /**
     * It fills fields for time training with training values.
     * @param exercise
     */
    private void timeBind(TimeExercise exercise) {
        timeText.setText(String.valueOf(exercise.getMinutes()));
    }

    /**
     * It fills fields for sets training with training values.
     * @param exercise
     */
    private void setsBind(SetsExercise exercise) {
        sets.setText(String.valueOf(exercise.getSets()));
        repeats.setText(String.valueOf(exercise.getReps()));
    }

    /**
     * It shows only fields for time training.
     */
    private void isTime() {
        hBoxDistance.setManaged(false);
        hBoxDistance.setVisible(false);
        hBoxSets.setManaged(false);
        hBoxSets.setVisible(false);
        vBoxTime.setManaged(true);
        vBoxTime.setVisible(true);
    }

    /**
     * It shows only fields for distance training.
     */
    private void isDistance() {
        hBoxDistance.setManaged(true);
        hBoxDistance.setVisible(true);
        hBoxSets.setManaged(false);
        hBoxSets.setVisible(false);
        vBoxTime.setManaged(false);
        vBoxTime.setVisible(false);
    }

    /**
     * It shows only fields for sets training.
     */
    private void isSets() {
        hBoxDistance.setManaged(false);
        hBoxDistance.setVisible(false);
        hBoxSets.setManaged(true);
        hBoxSets.setVisible(true);
        vBoxTime.setManaged(false);
        vBoxTime.setVisible(false);
    }
}
