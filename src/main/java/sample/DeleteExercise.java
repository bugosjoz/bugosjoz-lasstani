package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

import java.io.IOException;

/**
 * Created by jozef on 2.5.2017.
 */
public class DeleteExercise extends Stage {
    public DeleteExercise(Window window){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("deleteExercise.fxml"));
            setTitle("Delete exercise");
            setWidth(480);
            setHeight(220);
            initStyle(StageStyle.UTILITY);
            initModality(Modality.WINDOW_MODAL);
            setResizable(false);
            initOwner(window);
            setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
