package sample;

import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.*;
import javafx.stage.Window;

import java.io.IOException;
import static javafx.application.Application.launch;

public class Login extends Stage{

    public Login(){
        try {
            Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("loginPage.fxml"));
            setTitle("Login in");
            setWidth(500);
            setHeight(500);
            setScene(new Scene(root));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
