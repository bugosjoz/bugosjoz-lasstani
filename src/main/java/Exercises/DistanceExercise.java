package Exercises;

/**
 * Created by jozef on 22.4.2017.
 */
public class DistanceExercise extends Exercise {

    private int distance;
    private int minutes;
    private double averageSpeed;

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public double getAverageSpeed() {
        return minutes == 0 ? 0 :  (distance / (double) 1000) / (minutes / (double) 60);
    }


    public DistanceExercise(String name) {
        super(name);
    }

    public DistanceExercise(String name, int distance,int minutes)
    {
        this(name);
        setDistance(distance);
        setMinutes(minutes);
    }

}
