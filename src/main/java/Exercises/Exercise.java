package Exercises;

/**
 * Created by jozef on 22.4.2017.
 * Abstract class to store any kind of exercises
 */
public abstract class Exercise
{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Exercise(String name)
    {
        this.name = name;
    }

    @Override
    public String toString()
    {
        return name;
    }
}
