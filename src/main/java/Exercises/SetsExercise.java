package Exercises;

/**
 * Created by jozef on 22.4.2017.
 */
public class SetsExercise extends Exercise {

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    private int sets;
    private int reps;

    public SetsExercise(String name) {
        super(name);
    }

    public SetsExercise(String name,int reps,int sets) {
        this(name);
        setReps(reps);
        setSets(sets);
    }

}
