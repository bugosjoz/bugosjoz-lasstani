package Exercises;

import DB.Database;
import Exceptions.UserException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by jozef on 22.4.2017.
 */
public class ExerciseManager {
    private Database db;
    private int index;
    private ObservableList<Exercise> exercises;

    public ObservableList<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(ObservableList<Exercise> exercises) {
        this.exercises = exercises;
    }

    /**
     * Create connection with database and load exercises from database
     *
     * @throws SQLException
     * @throws UserException
     */
    public ExerciseManager(int index) throws SQLException,UserException
    {
        this.index = index;
        exercises = FXCollections.observableArrayList();
        db = Database.getInstance();
        Object[] columns = {"name","time","distance","sets"};
        ResultSet rs = db.select("exercises",columns,"exercise_id = ? OR exercise_id = ?",new Object[] {this.index,0});
        if (!rs.isBeforeFirst() )
            throw new SQLException("");
        while(rs.next()) {
            String name = rs.getString("name");
            boolean isSets = rs.getBoolean("sets");
            boolean isTime = rs.getBoolean("time");
            boolean isDistance = rs.getBoolean("distance");
            addExercise(name,isSets,isTime,isDistance,false);
        }

    }

    /**
     * It saves to the database a subject,
     * throws an exception if a problem occurs.
     * @param name
     * @param isSets
     * @param isTime
     * @param isDistance
     * @param addToDatabase
     * @throws UserException
     */
    public void addExercise(String name, boolean isSets, boolean isTime, boolean isDistance,boolean addToDatabase) throws UserException {
        if(name.equals(""))
            throw new UserException("Enter the name");
        String type = "";
        if (isSets) {
            exercises.add(new SetsExercise(name));
            type = "sets";
        } else if (isDistance) {
            exercises.add(new DistanceExercise(name));
            type = "distance";
        } else if (isTime) {
            exercises.add(new TimeExercise(name));
            type = "time";
        }
        if(!addToDatabase)
            return;
        String[] columns = {"name",type,"exercise_id"};
        Object[] params = {name,true,this.index};
        try {db.insert("exercises",columns,params);}
        catch(SQLException ex)
        {   throw new UserException("Exercise already exists.");}
    }

    /**
     * It deletes data form database,
     * throws an exception if a problem occurs.
     * @param name
     * @throws UserException
     * @throws SQLException
     */
    public void deleteExercise(String name) throws UserException,SQLException
    {
        if(name.equals(""))
            throw new UserException("Enter the name");
        Object[] params = {name,this.index};
        int num = db.delete("exercises","name = ? AND exercise_id = ?",params);
            if(num == 0)
                throw new UserException("Exercise doesn't exist.");
    }
}
