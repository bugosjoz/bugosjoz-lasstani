package Exercises;

import Exercises.Exercise;

/**
 * Created by jozef on 22.4.2017.
 */
public class TimeExercise extends Exercise {

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    private int minutes;

    public TimeExercise(String name) {
        super(name);
    }

    public TimeExercise(String name, int minutes)
    {
        this(name);
        setMinutes(minutes);
    }
}
