package Trainings;

import Client.Client;
import Exceptions.GeneralException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by jozef on 23.4.2017.
 */
public class TrainingDay {
    private LocalDate date;
    private ObservableList<Training> trainings;
    private Client client;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    private int index;

    public TrainingDay() throws SQLException,IOException,GeneralException
    {
        this.date = LocalDate.now();
        trainings = FXCollections.observableArrayList();
    }

    public TrainingDay(String name,LocalDate date) throws SQLException, IOException,GeneralException
    {
        this.name = name;
        this.date = date;
        trainings = FXCollections.observableArrayList();
        client = Client.getInstance();
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public ObservableList<Training> getTrainings() {
        return trainings;
    }

    public void addTraining(Training training)
    {
        trainings.add(training);
    }

    public void removeTraining(Training training)
    {
        trainings.remove(training);
    }

    public void resetTrainings()
    {
        trainings = FXCollections.observableArrayList();
    }

    public void saveTraining() throws IOException, GeneralException
    {
        client.save(name,date, trainings);
    }
    @Override
    public String toString()
    {
        return name + "(" + date + ")";
    }


}
