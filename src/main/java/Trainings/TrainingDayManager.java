package Trainings;

import DB.Database;
import Exceptions.GeneralException;
import Exceptions.UserException;
import Exercises.*;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by jozef on 23.4.2017.
 * Manages data from trainings, save/load them to/from database
 */
public class TrainingDayManager {

    private ExerciseManager exerciseManager;
    private TrainingDay trainingDay;
    private Database db;
    private int index;

    /**
     * Constructor, initialize of attributes.
     * @param index int
     * @throws SQLException
     * @throws UserException
     * @throws IOException
     * @throws GeneralException
     */
    public TrainingDayManager(int index) throws SQLException, UserException,IOException, GeneralException
    {
        this.index = index;
        this.exerciseManager = new ExerciseManager(index);
        trainingDay = new TrainingDay();
        db = Database.getInstance();
    }
    public TrainingDayManager(ExerciseManager exerciseManager) throws SQLException, UserException,IOException,GeneralException
    {
        this.exerciseManager = exerciseManager;
        trainingDay = new TrainingDay();
        db = Database.getInstance();
    }

    public ExerciseManager getExerciseManager() {
        return exerciseManager;
    }

    public void setExerciseManager(ExerciseManager exerciseManager) {
        this.exerciseManager = exerciseManager;
    }

    public TrainingDay getTrainingDay() {
        return trainingDay;
    }

    public void setTrainingDay(TrainingDay trainingDay) {
        this.trainingDay = trainingDay;
    }

    public void newTraining() throws SQLException, UserException,IOException, GeneralException
    {
        trainingDay = new TrainingDay();
    }


    /**
     * It saves training to database by type of exercise,
     * throws an exception if a problem occurs.
     * @param name String
     * @param date LocalDate
     * @throws SQLException
     */
    public void save(String name, LocalDate date) throws SQLException
    {
        String[] col = {"name","date","user_index"};
        Object[] par = {name,date,this.index};
        db.insert("training",col,par);
        for(Training training : trainingDay.getTrainings())
        {
            Exercise exercise = training.getCurrentExercise();
            if (exercise instanceof SetsExercise) {
                String[] columns = {"name","sets","reps","sets_index"};
                Object[] params = {exercise.getName(),((SetsExercise) exercise).getSets(),((SetsExercise) exercise).getSets(),name};
                db.insert("sets",columns,params);
            } else if (exercise instanceof DistanceExercise) {
                String[] columns = {"name","distance","minutes","distance_index"};
                Object[] params = {exercise.getName(),((DistanceExercise) exercise).getDistance(),((DistanceExercise) exercise).getMinutes(),name};
                db.insert("distance",columns,params);
            } else if (exercise instanceof TimeExercise) {
                String[] columns = {"name","minutes","time_index"};
                Object[] params = {exercise.getName(),((TimeExercise) exercise).getMinutes(),name};
                db.insert("time",columns,params);
            }
        }
        trainingDay.resetTrainings();
    }

    /**
     * It loads names of trainings from database,
     * throws an exception if a problem occurs.
     * @return names
     * @throws SQLException
     */
    public ResultSet loadNames() throws SQLException
    {
        Object[] columns = {"name","date"};
        return db.select("training", columns, "user_index = ?", new Object[] {this.index});
    }

    public void deleteName(String name) throws SQLException,UserException
    {
        if(name.equals(""))
            throw new UserException("Enter the name");
        Object[] params = {name};
        int num = db.delete("training","name = ?",params);
        if(num == 0)
            throw new UserException("Training doesn't exist.");
    }

    /**
     * It loads training data from database and save it to new object by type of exercise,
     * throws an exception if a problem occurs.
     * @param training_name String
     * @throws SQLException
     * @throws UserException
     * @throws IOException
     * @throws GeneralException
     */
    public void load(String training_name) throws SQLException, UserException,IOException, GeneralException
    {
        newTraining();
        Object[] columns = {"name","date"};
        ResultSet rs = db.select("training", columns, "name = ?",new Object[] {training_name});
        LocalDate date = null;
        if (!rs.isBeforeFirst() )
            throw new SQLException("");
        while(rs.next()) {
            date = rs.getDate("date").toLocalDate();
        }
        trainingDay.setName(training_name);
        trainingDay.setDate(date);
        rs = db.select("sets",new Object[] {"name","sets","reps"}, "sets_index = ?",new Object[] {training_name});
        while(rs.next()) {
            String name = rs.getString("name");
            int sets = rs.getInt("sets");
            int reps = rs.getInt("reps");
            trainingDay.addTraining(new Training(new SetsExercise(name,sets,reps)));
        }
        rs = db.select("distance",new Object[] {"name","distance","minutes"},"distance_index = ?",new Object[] {training_name});
        while(rs.next()) {
            String name = rs.getString("name");
            int distance = rs.getInt("distance");
            int minutes = rs.getInt("minutes");
            trainingDay.addTraining(new Training(new DistanceExercise(name,distance,minutes)));
        }
        rs = db.select("time",new Object[] {"name","minutes"},"time_index = ?",new Object[] {training_name});
        while(rs.next()) {
            String name = rs.getString("name");
            int minutes = rs.getInt("minutes");
            trainingDay.addTraining(new Training(new TimeExercise(name,minutes)));
        }
    }
    private boolean SaveTrainingAs() {
        return WriteTraining();
    }

    private boolean WriteTraining() {
        return true;
    }


}
