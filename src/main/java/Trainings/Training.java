package Trainings;

import Exercises.DistanceExercise;
import Exercises.Exercise;
import Exercises.SetsExercise;
import Exercises.TimeExercise;

/**
 * Created by jozef on 22.4.2017.
 * One exercise
 */
public class Training {
    private Exercise currentExercise;
    private int addedWeight;
    private String note;

    public Training(Exercise exercise)
    {
        currentExercise = exercise;
        addedWeight = 0;
        note = "";
    }

    public Exercise getCurrentExercise() {
        return currentExercise;
    }

    public void setCurrentExercise(Exercise currentExercise) {
        this.currentExercise = currentExercise;
    }

    public boolean isSets() {
        return currentExercise instanceof SetsExercise;
    }

    public boolean isTime() {
        return currentExercise instanceof TimeExercise;
    }

    public boolean isDistance() {
        return currentExercise instanceof DistanceExercise;
    }

    public boolean isAddedWeight() {
        return addedWeight != 0;
    }

    public int getAddedWeight() {
        return addedWeight;
    }

    public void setAddedWeight(int addedWeight) {
        this.addedWeight = addedWeight;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString()
    {
        return getCurrentExercise().getName();
    }
}
