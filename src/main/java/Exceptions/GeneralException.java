package Exceptions;

/**
 * Created by jozef on 7.5.2017.
 * General exception send from server
 */
public class GeneralException extends Exception {
    public GeneralException(String s)
    {
        super(s);
    }
}
