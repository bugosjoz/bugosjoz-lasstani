package Exceptions;

/**
 * Created by jozef on 24.4.2017.
 * Expection caused by wrong data entered by user
 */
public class UserException extends Exception {
    public UserException(String s)
    {
        super(s);
    }
}
