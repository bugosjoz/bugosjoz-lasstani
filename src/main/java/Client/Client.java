package Client;

import Exceptions.GeneralException;
import Exercises.DistanceExercise;
import Exercises.Exercise;
import Exercises.SetsExercise;
import Exercises.TimeExercise;
import Trainings.Training;
import Trainings.TrainingDay;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * Created by jozef on 6.5.2017.
 * Client who communicate with server via TCP and own protocol.
 * Messages to server are send according to Events from GUI.
 */
public class Client {

    private BufferedReader in;
    private PrintWriter out;

    public LocalDate getTrainingDate() {
        return trainingDate;
    }

    public void setTrainingDate(LocalDate trainingDate) {
        this.trainingDate = trainingDate;
    }

    private LocalDate trainingDate = LocalDate.now();

    public ObservableList<Exercise>  getExercises() {
        return exercises;
    }

    private ObservableList<Exercise> exercises;

    public ObservableList<TrainingDay> getTrainingDays() {
        return trainingDays;
    }

    private ObservableList<TrainingDay> trainingDays;

    public ObservableList<Training> getTrainings() {
        return trainings;
    }

    public void resetTrainings() {
        trainings = FXCollections.observableArrayList();
    }

    private ObservableList<Training> trainings;
    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    private boolean isLoggedIn;
    private static final Object mutex = new Object();
    private static Client instance = null;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) throws IOException {
        this.index = index;
    }

    private int index = -1;
    public static Client getInstance() throws IOException,GeneralException {
        Client tmp = instance;
        if (tmp == null) {
            synchronized (mutex) {
                tmp = instance;
                if (tmp == null) {
                        tmp = new Client();
                        instance = tmp;
                }
            }
        }
        return instance;
    }

    private Client() throws IOException,GeneralException
    {
        isLoggedIn = false;
        connectToServer();
    }

    /**
     * It loads basic exercises,
     * throws an exception if a problem occurs.
     * @throws IOException
     */
    private void initExercises() throws IOException {
        exercises = FXCollections.observableArrayList();
        out.println("INIT");
        while(true)
        {
            String name = in.readLine();
            if(name == null || name.equals("STOP"))
                break;
            String type = in.readLine();
            if(type == null || type.equals("STOP"))
                break;
            switch (type) {
                case "TIME":
                    exercises.add(new TimeExercise(name));
                    break;
                case "DISTANCE":
                    exercises.add(new DistanceExercise(name));
                    break;
                case "SETS":
                    exercises.add(new SetsExercise(name));
                    break;
                default:
                    throw new IOException();
            }
        }
    }

    /**
     * It creates connection to the server,
     * throws an exception if a problem occurs.
     * Socket = 9898;
     * url = localhost;
     * @throws IOException
     * @throws GeneralException
     */
    private void connectToServer() throws IOException,GeneralException {

        trainingDays = FXCollections.observableArrayList();
        resetTrainings();
        // Make connection and initialize streams
        Socket socket = new Socket("localhost", 9898);
        in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);
        String error = in.readLine();
        if(error.equals("CLOSED")) {
            throw new GeneralException("Server is full");
        }
        if(!error.equals("OK"))
        {
            throw new GeneralException("Cannoct connect to database.");
        }

    }

    /**
     * It logs in user according to the specified parameters,
     * throws an exception if a problem occurs.
     * @param name
     * @param password
     * @throws GeneralException
     * @throws IOException
     */
    public void logIn(String name, String password) throws GeneralException,IOException
    {
        out.println("LOGIN\n" + name + "\n" + password);
        String error = in.readLine();
        if(!error.equals("OK"))
        {
            throw new GeneralException(error);
        }
        this.index = Integer.parseInt(in.readLine());
        isLoggedIn = true;
        initExercises();
    }

    /**
     * It signs up user according to the specified parameters,
     * throws an exception if a problem occurs.
     * @param name
     * @param surname
     * @param username
     * @param mail
     * @param password
     * @param passwordAgain
     * @param birthday
     * @param male
     * @param height
     * @param weight
     * @param agree
     * @throws GeneralException
     * @throws IOException
     */
    public void doRegistration(String name, String surname, String username, String mail, String password, String passwordAgain, LocalDate birthday, boolean male, double height, double weight, boolean agree)
    throws GeneralException,IOException
     {
        String date = birthday == null ? "" : birthday.toString();
        out.println("REGISTRATION\n" + name + '\n' + surname + '\n' + username + '\n'
                    + mail + '\n' +
                password +
                '\n' + passwordAgain
                + '\n' + date
                    + '\n' + String.valueOf(male) + '\n' + String.valueOf(height) + '\n'
                    + String.valueOf(weight) + '\n' + String.valueOf(agree));
        String error = in.readLine();
        if(error == null || !error.equals("OK"))
        {
            throw new GeneralException(error);
        }
    }

    /**
     * It adds a new exercise to the database according to the specified parameters,
     * throws an exception if a problem occurs.
     * @param name
     * @param isTime
     * @param isDistance
     * @param isSets
     * @throws IOException
     * @throws GeneralException
     */
    public void addExercise(String name, boolean isTime, boolean isDistance, boolean isSets) throws IOException,GeneralException
    {
        out.println("ADD");
        out.println(name);
        String type = "";
        if (isSets) {
            exercises.add(new SetsExercise(name));
            type = "SETS";
        } else if (isDistance) {
            exercises.add(new DistanceExercise(name));
            type = "DISTANCE";
        } else if (isTime) {
            exercises.add(new TimeExercise(name));
            type = "TIME";
        }
        out.println(type);
        String error = in.readLine();
        if(error == null || !error.equals("OK"))
        {
            exercises.remove(exercises.size() - 1);
            throw new GeneralException(error);
        }
    }

    /**
     * It deletes the existing exercise from the database according to the specified parameters,
     * throws an exception if a problem occurs.
     * @param name
     * @param index
     * @throws IOException
     * @throws GeneralException
     */
    public void deleteExercise(String name,int index) throws IOException,GeneralException
    {
        out.println("DELETE");
        out.println(name);
        exercises.remove(index);
        String error = in.readLine();
        if(error == null || !error.equals("OK"))
        {
            throw new GeneralException(error);
        }
    }

    /**
     * According to the parameters, it finds the type of training and saves it into the database,
     * throws an exception if a problem occurs.
     * @param name
     * @param date
     * @param trainings
     * @throws IOException
     * @throws GeneralException
     */
    public void save(String name, LocalDate date, ObservableList<Training> trainings) throws IOException,GeneralException
    {
        out.println("SAVE");
        out.println(name + '\n' + date.toString());
        for(Training training : trainings)
        {
            Exercise exercise = training.getCurrentExercise();
            if (exercise instanceof SetsExercise) {
                out.println("S " + exercise.getName());
                out.println(((SetsExercise) exercise).getReps());
                out.println(((SetsExercise) exercise).getSets());
            } else if (exercise instanceof DistanceExercise) {
                out.println("D " + exercise.getName());
                out.println(((DistanceExercise) exercise).getDistance());
                out.println(((DistanceExercise) exercise).getMinutes());
            } else if (exercise instanceof TimeExercise) {
                out.println("T " + exercise.getName());
                out.println(((TimeExercise) exercise).getMinutes());
            }
        }
        out.println("STOP");
        String error = in.readLine();
        if(error == null || !error.equals("OK"))
        {
            throw new GeneralException(error);
        }
    }

    private int tryParse(String s)
    {
        int num;
        try {
            num = Integer.parseInt(s);
        }
        catch (NumberFormatException ex)
        {
            num = 0;
        }
        return num;
    }

    /**
     * It loads training by the name from database,
     * throws an exception if a problem occurs.
     * @param name
     * @throws IOException
     * @throws GeneralException
     */
    public void load(String name) throws IOException,GeneralException
    {
        trainings.clear();
        out.println("LOAD");
        out.println(name);
        LocalDate date = LocalDate.parse(in.readLine());
        setTrainingDate(date);
        while(true)
        {
            String s = in.readLine();
            if(s == null || s.equals("STOP"))
                break;
            //Sets training
            if(s.charAt(0) == 'S')
            {
                String exercise_name = s.substring(2);
                //save to sets table
                String sets,reps;
                sets = in.readLine(); reps = in.readLine();
                if(sets == null || reps == null)
                    throw new IOException();
                int intSets = tryParse(sets);
                int intReps = tryParse(reps);
                trainings.add(new Training(new SetsExercise(exercise_name,intReps,intSets)));
            }
            //Distance training
            else if(s.charAt(0) == 'D')
            {
                String exercise_name = s.substring(2);
                //save to distance table
                String distance,minutes;
                distance = in.readLine(); minutes = in.readLine();
                if(distance == null || minutes == null)
                    throw new IOException();
                int intDistance = tryParse(distance);
                int intMinutes = tryParse(minutes);
                trainings.add(new Training(new DistanceExercise(exercise_name,intDistance,intMinutes)));
            }
            //Time training
            else if(s.charAt(0) == 'T')
            {
                //save to time table
                String exercise_name = s.substring(2);
                String minutes;
                minutes = in.readLine();
                if(minutes == null)
                    throw new IOException();
                int intMinutes = tryParse(minutes);
                trainings.add(new Training(new TimeExercise(exercise_name,intMinutes)));
            }

        }

    }

    /**
     * It loads names of all users trainings from database,
     * throws an exception if a problem occurs.
     * @throws IOException
     * @throws SQLException
     * @throws GeneralException
     */
    public void loadTrainings()  throws IOException,SQLException,GeneralException{
        trainingDays.clear();
        out.println("LOAD_NAMES");
        while(true)
        {
            String name = in.readLine();
            if(name == null || name.equals("STOP"))
                break;
            if(name.contains("Error"))
                throw new GeneralException(name);
            LocalDate date = LocalDate.parse(in.readLine());
            if(date == null)
                break;
            trainingDays.add(new TrainingDay(name,date));
        }
        /*String error = in.readLine();
        if(error == null || !error.equals("OK"))
        {
            throw new GeneralException(error);
        }*/
    }

    public void deleteTraining(String name) throws IOException,GeneralException {
        out.println("DELETE_TRAINING");
        out.println(name);
        String error = in.readLine();
        if(error == null || !error.equals("OK"))
        {
            throw new GeneralException(error);
        }
    }
}
