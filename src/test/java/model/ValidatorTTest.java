package model;

import Exceptions.UserException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.*;

/**
 * Created by jozef on 25.5.2017.
 */
public class ValidatorTTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    @Test
    public void validatePassword() throws Exception {
        assertTrue(Validator.validatePassword("abcdefg", "abcdefg"));
        assertTrue(Validator.validatePassword("123abce", "123abce"));
    }

    @Test
    public void validateUserName() throws Exception {
        assertTrue(Validator.validateUserName("Name"));
        assertTrue(Validator.validateUserName("username2"));
        assertTrue(Validator.validateUserName("A958sdfdfdsf"));
    }

    @Test
    public void validateMail() throws Exception {
        assertTrue(Validator.validateMail("ahoj@gmail.com"));
        assertTrue(Validator.validateMail("a@gm.co"));
        assertTrue(Validator.validateMail("aa@azet.sk"));
    }

    @Test
    public void validateName() throws Exception {
        assertTrue(Validator.validateName("Jozef"));
        assertTrue(Validator.validateName("Stany"));
        assertTrue(Validator.validateName("Chujovina"));
    }

    @Test
    public void validateDate() throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-M-d");
        assertTrue(Validator.validateDate(LocalDate.now()));
        assertTrue(Validator.validateDate(LocalDate.parse("1995-5-11", formatter)));
        assertTrue(Validator.validateDate(LocalDate.parse("2002-11-9", formatter)));
        assertTrue(Validator.validateDate(LocalDate.parse("2016-9-11", formatter)));
    }

    @Test
    public void validateDouble() throws Exception {
        assertTrue(Validator.validateDouble(1.55, "height"));
        assertTrue(Validator.validateDouble(0.000000000000001, "weight"));
        assertTrue(Validator.validateDouble(155.55, "height"));
        assertTrue(Validator.validateDouble(90.55, "height"));

    }

    @Test
    public void validatePasswordTooShort() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The password must be long at least 6 characters.");
        assertTrue(Validator.validatePassword("abc", "abc"));
    }

    @Test
    public void validatePasswordTooShort5() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The password must be long at least 6 characters.");
        assertTrue(Validator.validatePassword("12345", "12345"));
    }

    @Test
    public void validatePasswordNotGiven() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The password field is empty.");
        assertTrue(Validator.validatePassword("", ""));
    }
    @Test
    public void validatePasswordNotSame() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Passwords do not match.");
        assertTrue(Validator.validatePassword("abcddsfg", "cdesssg"));
    }

    @Test
    public void validateUserNameTooShort() throws Exception {
        thrown.expect(UserException.class);
        thrown.expectMessage("The name must be long at least 3 characters.");
        assertTrue(Validator.validateUserName("ab"));
    }
    @Test
    public void validateUserNameTooShort1() throws Exception {
        thrown.expect(UserException.class);
        thrown.expectMessage("The name must be long at least 3 characters.");
        assertTrue(Validator.validateUserName("a"));
    }
    @Test
    public void validateUserNameNotGiven() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The name field is empty.");
        assertTrue(Validator.validateUserName(""));
    }
    @Test
    public void validateUserNameWrong() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The name contains unauthorized characters.");
        assertTrue(Validator.validateUserName("15sadsaf"));
        assertTrue(Validator.validateUserName("Jozef Bugos"));
        assertTrue(Validator.validateUserName("Bugoš"));
    }
    @Test
    public void validateUserNameWrongSpace() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The name contains unauthorized characters.");
        assertTrue(Validator.validateUserName("Jozef Bugos"));
    }
    @Test
    public void validateUserNameWrongChar() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The name contains unauthorized characters.");
        assertTrue(Validator.validateUserName("Bugoš"));
    }
    @Test
    public void validateNameWrong() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Wrong name!");
        assertTrue(Validator.validateName("15sadsaf"));
    }
    @Test
    public void validateNameWrongSpace() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Wrong name!");
        assertTrue(Validator.validateName("Jozef Bugos"));
    }

    @Test
    public void validateNameWrongChar() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Wrong name!");
        assertTrue(Validator.validateName("Jozef'"));
    }

    @Test
    public void validateDateNotGiven() throws Exception {
        thrown.expect(UserException.class);
        thrown.expectMessage("Choose date");
        assertTrue(Validator.validateDate(null));
    }
    @Test
    public void validateDateFuture() throws Exception {
        thrown.expect(UserException.class);
        thrown.expectMessage("Date can't be in the future");
        LocalDate futureDate = LocalDate.now().plusYears(2).plusMonths(5).plusDays(17);
        assertTrue(Validator.validateDate(futureDate));
    }

    @Test
    public void validateDoubleNotGiven() throws UserException
    {
        thrown.expect(UserException.class);
        thrown.expectMessage("Choose name");
        Validator.validateDouble(0,"name");
        //Validator.validateDouble(0,"name");
    }
    @Test
    public void validateDoubleMinus() throws UserException
    {
        thrown.expect(UserException.class);
        thrown.expectMessage("Must be greater than zero");
        Validator.validateDouble(-1.3,"name");
    }

    @Test
    public void validateMailNotGiven() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("The email field is empty.");
        assertTrue(Validator.validateMail(""));
    }
    @Test
    public void validateMailBad1() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Wrong email address");
        assertTrue(Validator.validateMail("a"));
    }
    @Test
    public void validateMailBad2() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Wrong email address");
        assertTrue(Validator.validateMail("a@"));
    }
    @Test
    public void validateMailBad3() throws UserException {
        thrown.expect(UserException.class);
        thrown.expectMessage("Wrong email address");
        assertTrue(Validator.validateMail("@.com"));
    }
}