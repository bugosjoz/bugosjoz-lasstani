package DB;

import org.junit.Test;

import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import static org.junit.Assert.*;

/**
 * Created by jozef on 25.5.2017.
 */
public class QueryTest {

    @Test
    public void delete() throws Exception {
        Query query = new Query();
        assertEquals("DELETE FROM table WHERE id = ?",query.delete("table").where("id = ?").getQuery());
    }

    @Test
    public void selectWhere() throws Exception {
        Query query = new Query();
        assertEquals("SELECT index FROM table WHERE index = ?",query.select(new Object[] {"index"}).from("table").where("index = ?").getQuery());
    }

    @Test
    public void select() throws Exception {
        Query query = new Query();
        assertEquals("SELECT index,users FROM table",query.select(new Object[] {"index","users"}).from("table").getQuery());

    }

    @Test
    public void update() throws Exception {
        Query query = new Query();
        assertEquals("UPDATE table SET name = ?,username = ?,password = ? WHERE index = ?",query.update("table").set(new String[] {"name","username","password"})
                                                                                         .where("index = ?").getQuery());
    }

    @Test
    public void insert() throws Exception {
        Query query = new Query();
        assertEquals("INSERT INTO table (index) VALUES(?);", query.insert("table",new String[] {"index"}).values(new Object[]{3}).getQuery());
    }


}